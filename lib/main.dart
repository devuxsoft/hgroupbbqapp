import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

  void main() {
    runApp(MaterialApp(
      home: MyApp(),
      debugShowCheckedModeBanner: false,
    ));
  }

  class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => new _MyAppState();
  }

  class _MyAppState extends State<MyApp> {
    bool _isSelected = false;

    void _radio() {
      setState(() {
        _isSelected = !_isSelected;
      });
    }

    Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

    Widget horizontalLine() => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: ScreenUtil().setWidth(120),
          height: 1.0,
          color: Colors.black26.withOpacity(.2),
        ),
      );

    @override
    Widget build(BuildContext context) {
      // TODO: implement build
      ScreenUtil.init(BoxConstraints(
        maxWidth: 750,
        maxHeight: 1334
      ), allowFontScaling: true);

      return new Scaffold(
         backgroundColor: Colors.white,
         resizeToAvoidBottomPadding: true,
         body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
               Padding(
                padding: EdgeInsets.only(top: 50.0),
              ),
              Expanded(
                child: Container(),
              ),
            ],
         ),
      );
    }

  }
