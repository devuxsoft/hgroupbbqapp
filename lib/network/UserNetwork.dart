import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:hgroupbbq/utils/utils.dart';
import 'package:hgroupbbq/utils/nothing.dart';
import 'package:hgroupbbq/utils/request_type.dart';
import 'package:hgroupbbq/utils/request_type_exception.dart';
import 'dart:convert';

class UserNetwork{
   final http.Client _client;

   UserNetwork(this._client);

   Future <http.Response> requestUser({@required dynamic params = Nothing, @required String path, 
            @required RequestType requestType}) async{
      try{
         switch (requestType) {
            case RequestType.POST:
               return _client.post(Utils.URL + path, headers: {"Content-Type": "application/json"}, body: json.encode(params));
            case RequestType.GET:
               return _client.get(Utils.URL + path);
            default: return throw RequestTypeNotFoundException('The HTTP request method is not found');
         }
         
      }catch(error){
      
      }
   }
}