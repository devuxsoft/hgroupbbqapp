class User {

   final String email;
   final String name;
   final String lastName;
   final String token;

   User({this.email, this.name, this.lastName, this.token});

   factory User.fromJson(Map<String, dynamic> json) => User(
      email: json["email"],
      name: json["name"],
      lastName: json["lastName"],
      token: json["token"]);

   Map<String, dynamic> toJson() =>
      {"email": email, "name": name, "lastName": lastName, "token": token};

}